// Soal No 1
/*
    Tulis code function di sini
*/

function teriak() {
    return "Halo Sanbers!";
}

console.log(teriak()) // "Halo Sanbers!" 


// Soal No 2
/*
    Tulis code function di sini
*/
function kalikan(pertama, kedua) {
    return pertama * kedua;
}

var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48


// Soal No 3
/* 
    Tulis kode function di sini
*/

function introduce(nama, umur, alamat, hobby) {
    return "Nama saya " + nama + ", umur saya " + umur + " tahun, alamat saya di " + alamat + ", dan saya punya hobby yaitu " + hobby + "!"
}
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "proGramming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 


