//Soal no 1

function range(startNum, finishNum) {
    var i;
    var hasil = [];
    if (startNum > finishNum) {
        for (i = startNum; i >= finishNum; i--) {
            hasil.push(i);
        }
    } else {
        for (i = startNum; i <= finishNum; i++) {
            hasil.push(i);
        }
    }
    return hasil;
}

// Code di sini

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


// Soal no 2

function rangeWithStep(startNum, finishNum, step) {
    var i;
    var hasil = [];

    if (startNum > finishNum) {
        for (i = startNum; i >= finishNum; i -= step) {
            hasil.push(i);
        }
    } else {
        for (i = startNum; i <= finishNum; i += step) {
            hasil.push(i);
        }
    }
    return hasil;
}
// Code di sini

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

//Soal no 3

function sum(awal, akhir, loncat) {
    var i;
    var hasil = 0;

    if (awal > akhir) {
        for (i = awal; i >= akhir; i -= loncat) {
            hasil += i;
        }
    } else {
        for (i = awal; i <= akhir; i += loncat) {
            hasil += i;
        }
    }
    return hasil
}
// Code di sini
console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0 

//Soal No 4
//contoh input
var i;
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Bola"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]


function dataHandling(inputan) {
    for (i = 0; i < inputan.length; i++) {
        console.log(`Nomor ID: ${inputan[i][0]}`);
        console.log(`Nama Lengkap: ${inputan[i][1]}`);
        console.log(`TTL: ${inputan[i][2]} ${inputan[i][3]}`);
        console.log(`Hobi: ${inputan[i][4]}`);
        console.log();
    }
}

dataHandling(input)

//Soal No 5

function balikKata(inputan) {
    var i;
    var hasil = "";

    for (index = inputan.length; index >= 0; index--) {
        hasil += inputan.charAt(index);
    }
    return hasil;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

// Soal no 6

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(inputan = []) {
    var namaDepan = inputan[1]
    var tglLahir = inputan[3].split("/")

    inputan.splice(1, 1, `${namaDepan} Elsharawy`)
    inputan.splice(2, 1, `Provinsi ${inputan[2]}`)
    inputan.splice(4, 1, "Pria", "SMA Internasional Metro")
    console.log(inputan);
    tglLahir[1] = namaBulan(tglLahir[1]);

    var susunTanggal = [tglLahir[0], tglLahir[1], tglLahir[2]]
    console.log(susunTanggal);

    console.log(tglLahir.join("-"));
    console.log(namaDepan);
}

function namaBulan(angkaBulan) {
    switch (Number(angkaBulan)) {
        case 01: namaBulan = "Januari";
            break;
        case 02: namaBulan = "Februari";
            break;
        case 03: namaBulan = "Maret";
            break;
        case 04: namaBulan = "April";
            break;
        case 05: namaBulan = "Mei";
            break;
        case 06: namaBulan = "Juni";
            break;
        case 07: namaBulan = "Juli";
            break;
        case 08: namaBulan = "Agustus";
            break;
        case 09: namaBulan = "September";
            break;
        case 10: namaBulan = "Oktober";
            break;
        case 11: namaBulan = "November";
            break;
        case 12: namaBulan = "Desember";
            break;
        default: break;
    }
    return namaBulan;
}
dataHandling2(input);
