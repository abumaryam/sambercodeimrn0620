# Intro to Array

1. Hari ini kita akan kembali belajar tipe data yang lain di Javascript yaitu array.
2. Materi Array sudah dapat dibaca di blog.sanbercode: 
https://blog.sanbercode.com/docs/kurikulum-react-native-mobile-apps-development/week-1/javascript-array/
3. Untuk yang ingin belajar lewat video bisa ditonton di youtube berikut: https://www.youtube.com/watch?v=wp-GjAM0ZK0&list=PLdMopq3Lfhpfr3I-h7Cf3y7G16SIBO_Ix&index=7&t=48s
4. Lanjutan materi: Array multidimensi, berikut ini video lanjutan untuk array multidimensi:
https://www.youtube.com/watch?v=-v_P4UTLQLY&list=PLdMopq3Lfhpfr3I-h7Cf3y7G16SIBO_Ix&index=7
5. Link tugas ada di blog.sanbercode:
https://blog.sanbercode.com/docs/kurikulum-react-native-mobile-apps-development/week-1-assignment/assignment-5-array/
6. Seperti biasa, batas pengumpulan Tugas Array ini adalah jam 08.00 WIB besok pagi. Kembali kami ingatkan untuk mengumpulkan link commit tugas Anda di sanbercode.com sebelum batas pengumpulan tugas berakhir.

== Sekian Materi Hari Ini - Semangat Coding ==


Berikut video rekaman Live Session Bagian 2, silahkan menyimak pembahasan pada Live Session ini, termasuk di dalamnya pembahasan materi loop, materi function, soal-soal tugas dan tanya jawab.

Link video Live Session React Native - Bagian 2:
https://youtu.be/3qxscf1NCRg

Seperti yang telah diketahui, setelah materi hari ke 5, akan diadakan quiz pekanan.
Untuk itu, silahkan mengisi form untuk memilih waktu quiz yang tersedia. Perlu diberitahukan bahwa quiz memiliki komposisi nilai 25% terhadap nilai akhir, harap diperhatikan.

Terima Kasih

Link form pilih waktu quiz: https://forms.gle/5FEPaYNuwkkH3jCT8