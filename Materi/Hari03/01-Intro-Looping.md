# Intro to Looping

1. Hari ini materi kita adalah Looping. Looping merupakan metode yang digunakan untuk mengulang suatu fungsi atau blok kode berdasarkan kondisi yang ditetapkan. Selama kondisi terpenuhi, maka fungsi yang didefinisikan tersebut akan kembali berulang.
2. Teman-teman bisa ikutin dokumentasinya di blog.sanbercode: 
https://blog.sanbercode.com/docs/kurikulum-react-native-mobile-apps-development/week-1/javascript-loop/
3. Tugas Looping dapat ditemukan di bagian tugas blog.sanbercode:
https://blog.sanbercode.com/docs/kurikulum-react-native-mobile-apps-development/week-1-assignment/assignment-3-looping/
4. Buat yang mau belajar lewat video bisa ditonton di sini: https://www.youtube.com/watch?v=dNngHZKyydg&list=PLdMopq3Lfhpfr3I-h7Cf3y7G16SIBO_Ix&index=5&t=0s
5. Seperti biasa, batas pengumpulan tugas kali ini adalah jam 08.00 WIB esok hari. Jangan lupa untuk mengumpulkan link commit tugas Anda di sanbercode.com.

== Sekian Materi Hari Ini - Terima Kasih ==

Berikut video rekaman Live Session kita kemarin, dipersilahkan bagi peserta untuk menyimak pembahasan pada Live Session Sesi 1, Mulai dari pendahuluan, informasi terkait bootcamp, materi command line & git, materi javascript, pembahasan soal tugas dan tanya jawab. 
Link video Live Session React Native - Bagian 1:
https://youtu.be/Z3v9kVnThDI