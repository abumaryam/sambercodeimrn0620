// di index.js
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Tulis code untuk memanggil function readBooks di sini
var time = 10000;
var i = 0;


function read(time, books) {
    readBooks(time, books[i], function (remainingTime) {
        time = remainingTime;
        i++;
        if (time > 0 && i < books.length) {
            read(time, books);
        }
    });
}

read(time, books);