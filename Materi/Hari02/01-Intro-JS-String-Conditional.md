# Intro Javascript, String, dan Conditional

1. Mulai hari ini kita akan masuk ke dunia Javascript. Javascript yaitu salah satu bahasa pemrograman terpopuler di dunia yang dipakai di hampir semua web modern bahkan sekarang bisa membuat mobile app dengan bahasa Javascript.
2. Ada beberapa pilihan untuk menjalankan script Javascript di antaranya lewat console browser, tools online seperti repl.it atau bisa juga dengan nodejs.
3. Kita akan jalankan javascript dengan bantuan NodeJS.
4. Nodejs adalah software berbahasa Javascript yang berjalan pada sisi server. Sedangkan biasanya Javascript sangat erat kaitannya dengan client (browser) . Tapi dengan Nodejs kita bisa coba jalankan kode javascript kita walaupun tidak melalui console di browser.
5. Pada Intro javascript bagian pertama ini kita akan mulai berkenalan dengan javascript, kamu bisa baca dokumentasi yang sudah dibuat di link berikut:
https://blog.sanbercode.com/docs/kurikulum-react-native-mobile-apps-development/week-1/javascript/
6. Setelah pengenalan Javascript, selanjutnya kita belajar tentang Kondisional atau Control Flow di Javascript. berikut linknya:
https://blog.sanbercode.com/docs/kurikulum-react-native-mobile-apps-development/week-1/javascript-conditional/
7. Setelah belajar conditional, lanjut ke bagian setelahnya yaitu String, dokumentasinya masih bisa kalian temukan pada link berikut:
https://blog.sanbercode.com/docs/kurikulum-react-native-mobile-apps-development/week-1/javascript-string-method/
8. Tugas hari ini adalah string dan conditional, sudah ada di halaman blog.sanbercode silahkan dicek kembali. Cara pengerjaannya adalah untuk bagian Tugas String dikerjakan dalam file string.js, kalau untuk Tugas Conditional dikerjakan di conditional.js. Tugas nya dapat dilihat pada link berikut: https://blog.sanbercode.com/docs/kurikulum-react-native-mobile-apps-development/week-1-assignment/assignment-2-string-conditional/
9. Jika ada yang ingin belajar lewat video silakan ditonton rekaman video berikut: Materi Hello Javascript + String https://www.youtube.com/watch?v=AW9sqMiErtg&list=PLdMopq3Lfhpfr3I-h7Cf3y7G16SIBO_Ix&index=3&t=0s
10. Lanjut dengan video rekaman materi conditional: https://www.youtube.com/watch?v=Du0TT0c5Bdo&list=PLdMopq3Lfhpfr3I-h7Cf3y7G16SIBO_Ix&index=3
11. Tambahan: Setelah melalui beberapa pertimbangan, maka mulai saat ini waktu pengumpulan tugas akan dibatasi sampai pada waktu penyampaian materi berikutnya, yaitu jam 08.00 WIB di keesokan harinya. Silahkan manfaatkan waktu ini dengan sebaik-baiknya.

== Sekian Materi Hari Ini, Happy Coding ==

IM React Native 0620, [16.06.20 16:34]
#IndonesiaMengoding
#LiveSession
Pengumuman: Malam ini insyaAllah akan diadakan live session menggunakan MS Teams pada jam 20.00 (peserta dapat mengikuti menggunakan browser PC/Laptop tanpa harus memiliki akun MS Teams), kita akan membahas materi 2 hari ini yaitu Command Line & Git dan Javascript String & Conditional. Harap menunggu link dibagikan di channel ini untuk dapat bergabung pada live session malam ini.

Live session ini akan direkam, sehingga bagi peserta yang berhalangan untuk ikut serta dapat melihat rekaman yang akan dibagikan kemudian.

Terima Kasih

IM React Native 0620, [16.06.20 19:48]
#IndonesiaMengoding
#LiveSession
Kita akan mulai live Session hari ini pada jam 20.00 WIB. Silahkan klik link berikut dan masuk melalui browser sebagai anonymous. Dan harap menunggu sampai waktu yang ditentukan.

https://teams.microsoft.com/l/meetup-join/19%3Ameeting_NDVlMTc1MzgtNGJjYS00MThmLTg3MTctZThlNjliMzQ0NGI1%40thread.v2/0?context=%7B%22Tid%22%3A%22316609f9-3899-4971-8f81-681df6d6a4f3%22%2C%22Oid%22%3A%22bef1f42d-c5b1-4c1b-bec3-97c59ab25c49%22%2C%22IsBroadcastMeeting%22%3Atrue%7D