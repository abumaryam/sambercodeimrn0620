# Intro Object Literals

1. Sekarang kita kembali ke materi. Setelah sebelumnya kita belajar tipe data array, hari ini kita akan pelajari tipe data yang sangat penting di Javascript yaitu Object.
2. Object adalah kumpulan data tidak berurut yang berisi pasangan key dan value (key and value pairs).
3. Kalau sebelumnya kita belajar Array, dimana dalam array, data ditumpuk dengan indeks berurut (mulai dari indeks 0) sedangkan pada Object data ditumpuk dengan memberi nama key pada setiap datanya
4. Materi bacaan tentang object silahkan dibaca-baca dengan seksama 
https://blog.sanbercode.com/docs/kurikulum-react-native-mobile-apps-development/week-2/javascript-object/
5. Tugas kali ini dapat dilihat di blog.sanbercode, Tugas 6 - Object : 
https://blog.sanbercode.com/docs/kurikulum-react-native-mobile-apps-development/week-2-assignments/assignment-2-object-literal/
6. Video yang bisa dijadikan referensi tambahan terkait struktur object dan cara membuat dan menggunakan object dapat dilihat di sini:
https://www.youtube.com/watch?v=RKsapPaUgww

== Sekian Materi Hari Ini, Tetap Belajar ==

IM React Native 0620, [22.06.20 18:45]
#IndonesiaMengoding
#LiveSession
Malam ini insyaAllah akan kembali diadakan live session menggunakan MS Teams pada jam 20.00 WIB (peserta dapat mengikuti menggunakan browser PC/Laptop tanpa harus memiliki akun MS Teams).

Kita akan membahas 2 materi yaitu Array dan Object dan masing-masing tugas, yaitu Tugas 5 - Array dan Tugas 6 - Object. Dan jika sempat akan ditambah dengan pembahasan Quiz Pekan 1.

Live session ini akan direkam, sehingga bagi peserta yang berhalangan untuk hadir dapat melihat rekaman yang akan dibagikan kemudian.

Terima Kasih

IM React Native 0620, [22.06.20 19:55]
#IndonesiaMengoding
#LiveSession
Pada jam 20.00 WIB, Live Session akan dimulai. Silahkan teman-teman bergabung pada live session malam ini menggunakan link berikut:

https://teams.microsoft.com/l/meetup-join/19%3Ameeting_ZWY1ZDJkNjEtOTQyZC00N2RhLThjMDEtZTYyNmNkMDJhOGMz%40thread.v2/0?context=%7B%22Tid%22%3A%22316609f9-3899-4971-8f81-681df6d6a4f3%22%2C%22Oid%22%3A%22bef1f42d-c5b1-4c1b-bec3-97c59ab25c49%22%2C%22IsBroadcastMeeting%22%3Atrue%7D