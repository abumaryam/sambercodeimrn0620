var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise 
var time = 10000;
var i = 0;


function read(time, books) {
    readBooksPromise(time, books[i])
        .then(function (timeRemaining) {
            time = timeRemaining;
            i++;
            if (time > 0 && i < books.length) {
                read(time, books);
            }
        })
        .catch(function (timeRemaining) {
            console.log("Waktu Habis");
        });
}

read(time, books);