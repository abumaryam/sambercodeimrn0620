# Intro Command Libne dan Git

0. Bagi yang belum punya akun gitlab, silakan daftar atau register dulu ke https://gitlab.com. 
Setelah daftar, untuk mendapatkan username akun Gitlab dapat ditemukan di pojok kanan atas, klik akun lalu akan muncul dropdown yang berisi menu akun, salah satunya adalah nama akun gitlab (biasanya diawali tanda "@")
1. Menjadi programmer selalu identik dengan penggunaan command line atau cmd/terminal karena kebanyakan program hanya bisa dijalankan dengan perintah melalui command prompt.
2. Command Line Interface pada windows biasa kita sebut dengan cmd atau command prompt atau windows powershell. Sedangkan di linux atau OSX disebut terminal.
3. Sebagai pendahuluan, mari berkenalan dengan beberapa perintah dasar yang sering digunakan. kamu bisa ikuti dokumentasi berikut: 
https://blog.sanbercode.com/docs/kurikulum-react-native-mobile-apps-development/week-1/day-1/
4. Di dalam dokumentasi tersebut dijelaskan bagaimana cara nya membuat folder baru, membuat file baru, pindah direktori, dan melihat isi direktori.
5. Software cmd yang disarankan untuk diinstall di windows yaitu cmder. Kalian bisa download dan install terlebih dahulu https://cmder.net/ . Install yang full version nya ya (full version sepaket dengan instalasi git)
6. Masuk ke materi Git. Git adalah teknologi versioning control. Dengan git dan git platform (seperti Github, Gitlab, dan lainnya) kita bisa menyimpan pekerjaan di cloud/internet dan mengatur perubahan-perubahannya dengan rapi bahkan bisa berkolaborasi dengan banyak developer lainnya.
7. Perintah git biasanya diberikan melalui terminal atau cmd. Untuk kali ini kita akan banyak praktek beberapa command git seperti git clone, git add, git commit, dan git push untuk mengumpulkan tugas selanjutnya.
8. Jika belum ter-install, silakan download dan install git terlebih dahulu di https://git-scm.com/downloads  . (Buat yang sudah install cmder Full version harusnya sudah terinstall git di dalamnya jadi tidak usah install git lagi). Dalam paket instalasi git juga terdapat Git bash atau terminal bawaan dari git. kalian juga bisa memakai git bash sebagai alternatif terminal cmd atau cmder. (bisa gunakan command git --version untuk mengetahui apakah git sudah terinstall atau belum)
9. Pada proses install akan dikonfirmasi untuk menginstall git bash. Git bash adalah command line atau terminal yang diberikan oleh git dan termasuk recommended untuk dipakai sebagai terminal default. (Opsional untuk dipilih)
10. Tugas untuk hari ini bisa dibaca di link berikut : 
https://blog.sanbercode.com/docs/kurikulum-react-native-mobile-apps-development/week-1-assignment/gitlab/
11. Di dokumentasi tersebut juga sudah tersedia Bagian Materi yang bisa kamu baca untuk pengumpulan tugas. https://blog.sanbercode.com/docs/kurikulum-react-native-mobile-apps-development/week-1/git/
12. Untuk tugas hari ini dan selanjutnya akan dikumpulkan melalui akun sanbercode.com masing-masing peserta. Dan tata cara pengumpulan tugas dapat dilihat pada halaman berikut https://blog.sanbercode.com/docs/kurikulum-react-native-mobile-apps-development/introduction/proses-pengumpulan-tugas/
13. Berikut tambahan video penjelasan yang sangat mencerahkan dari Pak Sandika Galih - Web Pemrograman UNPAS:
https://youtu.be/lTMZxWMjXQU
14. Tambahan: Pada bootcamp kali ini, batas pengumpulan tugas adalah jam 24.00 WIB di hari yang sama, harap diperhatikan.
15. Beberapa tugas yang di submit telah diperiksa, tolong perhatikan proses pengumpulan tugas yang ada di poin 12. Terkait penilaian akan segera menyusul.

IM React Native 0620, [15.06.20 13:23]
== Sekian Materi Hari Ini, Terima Kasih ==