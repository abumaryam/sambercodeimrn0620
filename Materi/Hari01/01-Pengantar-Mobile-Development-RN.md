1. Sesuai timeline, untuk sekarang saya akan coba berikan pengantar dulu agar teman-teman bisa paham tentang peta perjalanan materi dan peran dari masing-masing materi dalam mengantarkan kita menjadi seorang mobile App Developer.
2. Javascript merupakan bahasa pemrograman terpopuler di planet Bumi berdasarkan jumlah penggunaannya di “medsos-nya” programmer: github.
3. Hal ini karena javascript merupakan salah satu bahasa pemrograman yang relatif mudah dipelajari. (sebagai sumber : https://pin.it/lhrcfdyu4fzpab )
4. Di dalam dunia mobile app development, tentu kita tahu dua raksasa OS pada perangkat mobile yaitu Android dan iOS.
5. Android secara aslinya menggunakan bahasa pemrograman Java, sedangkan iOS menggunakan Objective C. Berdasarkan gambar di atas, kedua bahasa tersebut tergolong sulit untuk dipelajari.
6. Membangun sebuah aplikasi mobile android menggunakan bahasa aslinya menjadikan kita membutuhkan semua bahasa yang menunjang dengan platform yang ada.
7. Terlebih lagi developer yang mengerjakannya akan dibagi dengan bahasa dan keahlian di masing-masing bahasa native.
8. Untuk itu lahirlah pemrograman hybrid sebagai alternatif teknologi untuk membangun sebuah aplikasi mobile selain menggunakan native.
9. Hybrid app memungkinkan kita mengembangkan satu aplikasi yang dapat digunakan ke semua perangkat (multi-platform). Salah satu pemrograman hybrid yang paling populer sekarang adalah React-Native.
10. Dengan React-Native, kita dapat keuntungan mengembangkan aplikasi mobile tanpa harus belajar bahasa native Android dan iOS. Kalau bisa keduanya, kenapa tidak?
11. Sebelum kita mempelajari React-Native yang menjadi “raja terakhir” dalam training Mobile App Development Sanbercode, terdapat beberapa hal yang perlu dipelajari sebagai dasar.
12. Untuk bisa memahami framework React Native, kita perlu belajar tentang Javascript dasar. Kita akan berkenalan dengan tipe data, kondisional, looping, function, dll di dalam Javasript. Selama pembelajaran javascript dasar kita akan sering menggunakan console atau lewat terminal dengan nodejs untuk menjalankan code.
13. Memasuki minggu ke-3, kita akan mulai fokus ke materi React Native. React Native adalah library Javascript yang dibuat oleh developer facebook untuk membangun User Interface.
14. Berbeda dengan React JS, yang digunakan pada Web Development, pada React Native terdapat beberapa library dan component tambahan yang dipergunakan untuk membuat aplikasi mobile. Untuk bisa mempelajari React Native juga dibutuhkan standar bahasa javascript ES6.
15. Di React Native kita akan berkenalan dengan Component. Analoginya seperti element yang terdapat pada HTML.
16. Di dalam setiap komponen sendiri terdapat API (Application Programming Interface) dan Lifecycle yang akan memudahkan konfigurasi UI sehingga menghasilkan performa yang maksimal.
17. Setelah komponen berhasil dibangun, selanjutnya adalah menyusun skema navigasi antar komponen, yang pada bootcamp ini menggunakan React Navigation.
18. Selain itu juga diperlukan pengaturan dalam pendistribusian data antar Component di React Native, sehingga perlu dipelajari mengenai state management, dimana state management yang akan kita pelajari yaitu Redux.

IM React Native 0620, [15.06.20 08:04]
Pengantar Mobile Development RN dicukupkan, mari kita mulai bootcamp kita.