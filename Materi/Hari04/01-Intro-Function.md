# Intro to Function

1. Hari ini kita akan berkenalan dengan Function. Secara sederhana, fungsi merupakan suatu blok kode yang dikumpulkan untuk menjalankan suatu perintah. Dan ia didefinisikan / diberi nama sehingga dapat digunakan dimanapun sesuai dengan lingkup fungsi itu berada.
2. Untuk lebih lengkapnya, teman-teman bisa ikutin dokumentasinya di blog.sanbercode: 
https://blog.sanbercode.com/docs/kurikulum-react-native-mobile-apps-development/week-1/javascript-function/
3. Tugas Function dapat dibaca di bagian tugas di blog.sanbercode:
https://blog.sanbercode.com/docs/kurikulum-react-native-mobile-apps-development/week-1-assignment/assignment-4-function/
4. Video Tutorial Functions:
https://www.youtube.com/watch?v=Zy3L02OITRo&list=PLdMopq3Lfhpfr3I-h7Cf3y7G16SIBO_Ix&index=5
5. Seperti biasa, batas pengumpulan Tugas Function ini adalah jam 08.00 WIB besok pagi. Kembali kami ingatkan untuk mengumpulkan link commit tugas Anda di sanbercode.com sebelum batas pengumpulan tugas berakhir.

Live session ini akan direkam, sehingga bagi peserta yang berhalangan untuk hadir dapat melihat rekaman yang akan dibagikan kemudian.

Terima Kasih

https://teams.microsoft.com/l/meetup-join/19%3Ameeting_NTljNjg1YWQtNjJlNC00ODhkLWI3ZmEtZjNkMGE3NWJkZDIw%40thread.v2/0?context=%7B%22Tid%22%3A%22316609f9-3899-4971-8f81-681df6d6a4f3%22%2C%22Oid%22%3A%22bef1f42d-c5b1-4c1b-bec3-97c59ab25c49%22%2C%22IsBroadcastMeeting%22%3Atrue%7D