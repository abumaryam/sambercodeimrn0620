# SanbercodeIMRN0620

Selamat datang di Repository tempat saya submit tugas dari Sanbercode sekaligus untuk saya menuliskan materi yang telah disampaikan oleh instruktur sanbercode. Materi ini bebas untuk diakses publik.

## Daftar Isi Materi

### Hari ke 01
* [Pengantar](Materi/Hari01/00-Pengantar.md)
* [Pengantar Mobile Development React Native](Materi/Hari01/01-Pengantar-Mobile-Development-RN.md)
* [Intro Command Line dan Git](Materi/Hari01/02-Intro-command-line-dan-git.md)

### Hari ke 02
[Introduction to Javascript, String and Conditional](Materi/Hari02/01-Intro-JS-String-Conditional.md)

### Hari ke 03
[Introduction to Looping](Materi/Hari03/01-Intro-Looping.md)

### Hari ke 04
[Introduction to Function](Materi/Hari04/01-Intro-Function.md)

### Hari ke 05
[Introduction to Array](Materi/Hari05/01-Intro-Array.md)

