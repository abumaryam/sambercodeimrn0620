# Pengantar

Assalamu'alaikum, 
Kami hendak menginformasikan bahwa pada sore hari ini, insya Allah sekitar jam 16.00
kita akan memulai kegiatan bootcamp kali ini dengan briefing/kickoff yang membahas singkat tentang hal-hal yang berkaitan dengan kegiatan bootcamp.

Selanjutnya perlu kami informasikan juga bahwa telah dibuatkan grup diskusi (telegram) bagi seluruh peserta bootcamp.

Silahkan login ke "https://sanbercode.com/login" atau cari menu "member area" di sanbercode.com
untuk mendapatkan link untuk bergabung ke grup diskusi telegram.

## Informasi Tambahan,
Briefing/kickoff akan disampaikan melalui channel ini, jadi peserta tidak harus stand by untuk menunggu waktu briefing/kickoff.
Terima Kasih.
1. Assalamu’alaikum, Kami Tim Sanbercode mengucapkan Selamat Datang kepada peserta Bootcamp di kelas React Native Mobile Apps Development.
2. Pada Bootcamp kali ini kita akan mempelajari dari awal tentang bagaimana membuat suatu aplikasi mobile menggunakan salah satu framework populer saat ini yaitu React Native.
3. Dengan mengikuti seluruh materi, tugas, dan quiz yang diberikan pada kelas ini, peserta akan mendapatkan pemahaman menyeluruh terkait proses pembuatan aplikasi mobile sekaligus menambah portofolio sebagai React Native Developer.
4. Dan untuk peserta yang bisa menyelesaikan dengan baik seluruh tugas dan quiz yang diberikan, berhak memperoleh sertifikat.
5. Berikut timeline bootcamp kali ini.
6. Bootcamp kali ini berlangsung selama 4 pekan dan dibagi menjadi 2 kelompok materi, yaitu Javascript dan React Native.
7. Pada 2 pekan pertama, peserta akan mempelajari materi Javascript dasar mulai dari variabel, tipe data, dan berbagai fitur yang dimiliki oleh Javascript yang perlu dipahami sebelum mempelajari framework React Native.
8. Selanjutnya pada 2 pekan terakhir, peserta akan mempelajari dan mendalami framework React Native termasuk beberapa API dan library yang umum digunakan dalam mengerjakan proyek React Native.
9. Diharapkan setiap peserta mampu meluangkan waktunya minimal 2-3 jam per hari untuk fokus mengikuti materi dan mengerjakan tugas/quiz yang diberikan.
10. Selama mengikuti bootcamp ini setiap peserta diharapkan memiliki 2 perangkat utama yaitu Laptop/PC (minimal memiliki RAM 4 GB) dan smartphone Android (minimal Android version 5.0 Lollipop).
11. Smartphone Android akan digunakan sebagai media testing/simulasi selama masa pengembangan aplikasi mobile.
12. Dan melakukan instalasi beberapa perangkat lunak di Laptop/PC, antara lain: 
Telegram Desktop atau Telegram Web untuk memudahkan komunikasi antara trainer dan peserta maupun antar peserta, baik berupa tanya jawab maupun saat penyampaian dan pengerjaan tugas. (https://desktop.telegram.org/).
13. Dan text editor : Disarankan untuk memilih di antara Visual Studio Code, Sublime Text atau Atom. Tidak disarankan menggunakan Notepad atau notepad++. (https://code.visualstudio.com/).
14. Beberapa perangkat lunak lainnya akan disampaikan bersamaan dengan materi yang terkait.
15. Materi akan disampaikan dalam bentuk artikel, video tutorial, dan video conference (live/webinar). Dan insya allah akan disampaikan setiap hari Senin - Jumat mulai jam 08.00.
16. Setiap harinya peserta diminta untuk menyelesaikan tugas yang diberikan.Dan pada setiap akhir pekan, peserta diminta untuk mengikuti dan menyelesaikan quiz yang diberikan.
17. Apabila peserta menghadapi kesulitan dalam mengerjakan tugas diharapkan untuk terlebih dahulu memeriksa kembali soal dan materi yang berkaitan dengan tugas yang diberikan.
18. Jika masih merasa sulit, peserta bisa menggunakan Google search (atau search engine lainnya) untuk mencari solusi atas permasalahan yang dihadapi. Contoh keyword yang bisa digunakan adalah {teknologi/bahasa pemrograman/framework/library} + {masalah/kasus} dalam bahasa inggris, misal: javascript how to get object key.
19. Jika masih belum menemukan solusi, maka peserta dapat bertanya di grup diskusi telegram.
20. Grup diskusi disediakan oleh tim sanbercode untuk membicarakan permasalahan yang berkaitan dengan materi atau tugas yang kurang dimengerti selama kegiatan bootcamp berlangsung. Dan peserta disarankan untuk ikut berpartisipasi dan saling membantu jika mampu menjawab pertanyaan yang ada (kecuali jawaban tugas dan quiz).
21. Untuk itu mohon seluruh anggota grup tetap menjaga sikap dan perilaku di dalam grup diskusi agar bootcamp dapat berjalan dengan maksimal dan kondusif.
22. Selama kegiatan bootcamp terdapat beberapa trainer yang siap membimbing peserta untuk dapat memahami seluruh materi yang ada. Namun perlu diperhatikan bahwa trainer tidak bisa selalu stand by dan secara instan menjawab seluruh pertanyaan yang ada setiap saat.
23. Untuk itu para trainer berkomitmen untuk bisa memberikan fast response pada setiap hari Senin - Jumat pada jam 16.00-18.00 dan jam 20.00 - 22.00 WIB. Silahkan manfaatkan waktu tersebut dengan sebaik-baiknya.
24. Kami cukupkan kickoff untuk saat ini. Semoga bootcamp ini memberikan manfaat sebesar-besarnya untuk kita semua. Salam Sanber!