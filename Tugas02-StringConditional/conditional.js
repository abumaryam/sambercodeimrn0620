// Soal no 1

// Briefing: Terdapat tiga peran yaitu dosen, mahasiswa, dan operator.


var nama = "John"
var peran = ""

// Output untuk Input nama = '' dan peran = ''
if (nama == "" && peran == "") console.log("Nama harus diisi!");

//Output untuk Input nama = 'John' dan peran = ''
else if (nama != "" && peran == "") console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");

//Output untuk Input nama = 'Jane' dan peran 'Dosen'
else if (nama != "" && peran == "Dosen") console.log("Selamat datang di Dunia Kampus,  " + nama + "\n Halo Dosen " + nama + ", kamu dapat melihat siapa yang menjadi operator akademik !");

//Output untuk Input nama = 'Jenita' dan peran 'Mahasiswa'
else if (nama != "" && peran == "Mahasiswa") console.log("Selamat datang di Dunia Kampus,  " + nama + "\n Halo Mahasiswa " + nama + ", kamu akan membantu teman-temanmu berhadapan dengan operator akademik yang sulit ditemui");

//Output untuk Input nama = 'Junaedi' dan peran 'Operator'
else if (nama != "" && peran == "Operator") console.log("Selamat datang di Dunia Kampus,  " + nama + "\n Halo Operator " + nama + ", kamu akan membuat risih dosen dan mahasiswa jika tidak ada di meja kerja pada jam kerja pada saat dibutuhkan pelayanan akademik");

// Soal No 2

var hari = 21;
var bulan = 1;
var tahun = 1441;

switch (bulan) {
    case 1:
        var namaBulan = "Muharram";
        break;
    case 2:
        var namaBulan = "Safar";
        break;
    case 3:
        var namaBulan = "Rabi\'ul Awal";
        break;
    case 4:
        var namaBulan = "Rabi\'ul Akhir";
        break;
    case 5:
        var namaBulan = "Jumadil Awal";
        break;
    case 6:
        var namaBulan = "Jumadil Akhir";
        break;
    case 7:
        var namaBulan = "Rajab";
        break;
    case 8:
        var namaBulan = "Sya\'ban";
        break;
    case 9:
        var namaBulan = "Ramadhan";
        break;
    case 10:
        var namaBulan = "Syawal";
        break;
    case 11:
        var namaBulan = "Dzulqa\'dah";
        break;
    case 12:
        var namaBulan = "Dzulhijah";
        break;
    default:
        var namaBulan = null;
        break;
}

console.log(hari + " " + namaBulan + " " + tahun);